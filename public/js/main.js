//
//  Function for load content from url and put in $('.ajax-content') block
//
function ImportContent(tags,import_type,page){
	//$('.preloader').show();
	//if(page == '' )page = '';
	if(import_type == 'flickr'){page = parseInt(page);url = root + '/api/v1/flickr/search?tags=' + tags + '&limit=30&page=' + page; }
	else if(import_type == 'instagram')url = root + '/api/v1/instagram/search?tags=' + tags + '&limit=30&page=' + page;
	else if(import_type == 'youtube'){ page = page == 1 ? '' :  page ; url = root + '/api/v1/youtube/search?tags=' + tags + '&limit=30&next_page_token=' + page ; }
	else url = root + '/api/v1/flickr/search?tags=' + tags + '&limit=20&page=' + page;

	$("#loadmore-import-content").html('<b>Please wait loading...</b>'); //show when submitting
	$('body').addClass('stop-scrolling');


	$.ajax({
		url: url,
		type: 'GET',
		success: function(data) {
			var html_str = '';
			try{
				if(import_type == 'flickr'){
					html_str = ParseFlickr(data);page += 1;
				}
				else if(import_type == 'instagram'){
					html_str = ParseInstagram(data);
					page = data.results.pagination.next_max_id;
				}
				else if(import_type == 'youtube'){
					html_str = ParseYoutube(data);
					page = data.results.nextPageToken;
				}
				else{
					html_str = parseFlickr(data);
					page += 1;
				}
				
				$('#loadmore-import-content').remove();
				html_str += '<div class="col-sm-12" id = "loadmore-import-content" style = "text-align:center" page = "' + page + '" ><a href = "javascript:void(0)" onclick = \"ImportContent(' + "'" + tags + "'" + ',\'' + import_type + '\',' + page +  ')\">Load more</a></div>';
				$("#import-content").append(html_str);
				$('body').removeClass('stop-scrolling');
				//$('.preloader').hide();
			}catch(e){
				alert("No result found. Try again. Error : " + e.message);
				$('body').removeClass('stop-scrolling');
				$('#loadmore-import-content').remove();
				//$('.preloader').hide();
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
			$('body').removeClass('stop-scrolling');
			$('#loadmore-import-content').remove();
			//$('.preloader').hide();
		},
		dataType: "json"
		/*cache:false,
		headers: { "cache-control": "no-cache" }*/
	});
}//end function

$('#tags').keypress(function (e){
		if(e.which == 13){$("#import-content").html('');ImportContent($('#tags').val(),$('input[name=import_type]').val(),1)};
		if(e.which === 32)return false;
});




//parse flickr
function ParseFlickr(data){
	var html_str = '';
	$.each(data.results.photos.photo, function(i,item){
		if( typeof data.results.photos.photo[i]["@attributes"].url_n != 'undefined'  &&  typeof data.results.photos.photo[i]["@attributes"].url_c != 'undefined' &&  data.results.photos.photo[i]["@attributes"].media == 'photo' ){

			var link_url =  'http://www.flickr.com/photos/' +  data.results.photos.photo[i]['@attributes'].owner + '/' + data.results.photos.photo[i]['@attributes'].id ;

			html_str += '<div class="m12 col-sm-6 mbox"><div class="pull-left col-sm-4 "><a href = "' + data.results.photos.photo[i]["@attributes"].url_c + '" target = "_blank"><img  src="' + data.results.photos.photo[i]["@attributes"].url_n + '" width = "160" height = "160" /></a></div><div class="pull-right  col-sm-8"><textarea class="form-control" rows="7"  name = "messages[]" >' + data.results.photos.photo[i]["@attributes"].title  + '</textarea></div><div  class = "clearfix"></div><div class="col-sm-12 "><button type="button" class="btn btn-primary btn-block" onclick = "SelectItem(this)">Select</button><input type = "hidden" name = "img_urls[]"  value = "' + data.results.photos.photo[i]["@attributes"].url_c + '" /><input type = "hidden" name =  "src_urls[]" value = "' + link_url + '" /><input type = "hidden" name = "thmb_urls[]" value = "' + '' + '" /></div></div>';
			//console.log(data.results.photos.photo[i]["@attributes"].farm)
		}//end if
	});
	return html_str;
}//end function

//parse instragram
function ParseInstagram(d){
	var html_str = '';
	$.each(d.results.data, function(i,item){
		if( d.results.data[i].type == 'image'){
			var txt = d.results.data[i].caption == null ? '' : d.results.data[i].caption.text;
			html_str += '<div class="m12 col-sm-6 mbox"><div class="pull-left col-sm-4 "><a href = "' + d.results.data[i].images.standard_resolution.url  + '" target = "_blank"><img  src="' + d.results.data[i].images.thumbnail.url + '" width = "160" height = "160" /></a></div><div class="pull-right  col-sm-8"><textarea class="form-control" rows="7"  name = "messages[]" >' + txt  + '</textarea></div><div  class = "clearfix"></div><div class="col-sm-12 "><button type="button" class="btn btn-primary btn-block" onclick = "SelectItem(this)">Select</button><input type = "hidden" name = "img_urls[]"  value = "' + d.results.data[i].images.standard_resolution.url + '" /><input type = "hidden" name =  "src_urls[]" value = "' + d.results.data[i].link + '" /><input type = "hidden" name = "thmb_urls[]" value = "' + '' + '" /></div></div>';
			//console.log(data.results.photos.photo[i]["@attributes"].farm)
		}//end if
	});
	return html_str;
}//end function

//parse youtube
function ParseYoutube(d){
	var html_str = '';
	//console.log(d.results.items[0]);
	//console.log($('#import-content .mbox').length)
	//var mbox = $('#import-content .mbox').length;
	$.each(d.results.items, function(i,item){
		//mbox++;
		var video_id =  d.results.items[i].id.videoId;
		var video_url = 'http://www.youtube.com/v/' + video_id;
		var src_url = 'http://www.youtube.com/watch?v=' + video_id;
		
		html_str += '<div class="m12 col-sm-6 mbox' + '' + '"><div class="pull-left col-sm-4 "><a href = "' + video_url + '" target = "_blank"><img  src="' + d.results.items[i].snippet.thumbnails.medium.url + '" width = "160" height = "160" /></a></div><div class="pull-right  col-sm-8"><textarea class="form-control" rows="7" name = "messages[]" >' + d.results.items[i].snippet.title + '</textarea></div><div  class = "clearfix"></div><div class="col-sm-12 "><button type="button" class="btn btn-primary btn-block" onclick = "SelectItem(this)">Select</button><input type = "hidden" name = "img_urls[]"  value = "' + video_url + '" /><input type = "hidden" name =  "src_urls[]" value = "' + src_url + '" /><input type = "hidden" name = "thmb_urls[]" value = "' + video_url + '" /></div></div>';
		//console.log(data.results.photos.photo[i]["@attributes"].farm)
	});
	return html_str;
}//end function


//img_urls,src_urls,thmb_urls,messages,streams_ids,user_id,post_type
function PostContent(){
	console.log($('.selectbox input, .selectbox textarea, #networks' ).serialize());
	$('.preloader').show();
	var import_type = $('input[name=import_type]').val();
	var user_id = $('input[name=user_id]').val();
	var post_type = import_type == 'youtube' ? 'video' :  'photo';
	var url = root + '/api/v1/facebook/post';
	$.ajax({
		url: url,
		type: 'POST',
		data: $('.selectbox input, .selectbox textarea, #networks' ).serialize() + '&post_type=' + post_type,
		success: function(data) {
			try{
				$('#success_msg_instant').show();
				createAutoClosingAlert(".alert-success", 2000);
				console.log('SUCCESS : ' + data);
				$('.preloader').hide();
			}catch(e){
				alert('Error : ' + e.message);
				$('.preloader').hide();
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
			$('.preloader').hide();
		},
		cache:false,
		headers: { "cache-control": "no-cache" }
	});
}//end function


//img_urls,src_urls,thmb_urls,messages,streams_ids,user_id,post_type
function ScheduleContent(schedule_time){
	console.log($('.selectbox input, .selectbox textarea, #networks' ).serialize());
	var import_type = $('input[name=import_type]').val();
	var user_id = $('input[name=user_id]').val();
	var post_type = import_type == 'youtube' ? 'video' :  'photo';
	var url = root + '/api/v1/facebook/post';
	$.ajax({
		url: url,
		type: 'POST',
		data: $('.selectbox input, .selectbox textarea, #networks' ).serialize() + '&post_type=' + post_type + '&publish=false&publish_time=' + schedule_time,
		success: function(data) {
			try{
				console.log('SUCCESS : ' + data);
				$('#schdule-button').html('Schedule');
			}catch(e){
				$('#schdule-button').html('Schedule');
				alert('Error : ' + e.message);
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
			$('#schdule-button').html('Schedule');04-05-2014
		},
		cache:false,
		headers: { "cache-control": "no-cache" }
	});
}//end function




function createAutoClosingAlert(selector, delay) {
	var alert = $(selector).alert();
	window.setTimeout(function() { $(selector).fadeTo(500, 0).slideUp(500);}, delay);
}


function SelectItem(obj){
	if($(obj).html()== 'Select'){
		$(obj).parent().parent().addClass('selectbox');
		$(obj).removeClass('btn-primary');
		$(obj).addClass('selcbtn');
		$(obj).html('Unselect');
	}else{
		$(obj).parent().parent().removeClass('selectbox');
		$(obj).addClass('btn-primary');
		$(obj).removeClass('selcbtn');
		$(obj).html('Select');
	}

	if($('.selectbox').length > 0 )
		$('#allbutton').show();
	else
		$('#allbutton').hide();
}//end function

function ResetItem(){
	$('.mbox').removeClass('selectbox');
	$('.selcbtn').addClass('btn-primary');
	$('.selcbtn').html('Select');
	$('#allbutton').hide();
	return true;
}//end function



function DemoSelect2(){
	$('#networks').select2({placeholder: "Select Networks"});
}

$(document).ready(function() {
	LoadSelect2Script(DemoSelect2);
});


//add streams
function AddStream(network_id){
	var url = root + '/api/v1/streams';
	$('.preloader').show();
	$('#success_msg_instant').hide();
	$.ajax({
		url: url,
		type: 'POST',
		data: 'network_id=' + network_id + '&type=page',
		success: function(data) {
			try{
				console.log(data);
				$('#success_msg_instant').show();
				createAutoClosingAlert(".alert-success", 2000);
				console.log('SUCCESS : ' + data);
				$('.preloader').hide();
				$('#success_msg_instant').show();
				alert('Page imported successfully');
			}catch(e){
				alert('Error : ' + e.message);
				$('.preloader').hide();
				$('#success_msg_instant').hide();
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
			$('.preloader').hide();
			$('#success_msg_instant').hide();
		},
		cache:false,
		headers: { "cache-control": "no-cache" }
	});
}//end function


function RefreshNetwork(network_id,uid,id){
	
	var url = root + '/network/refresh/' + network_id + '/' + uid;
	$('#' + id).html('Refreshing...');
	$.ajax({
		url: url,
		type: 'GET',
		success: function(data) {
			try{
				console.log('SUCCESS : ' + data);
				$(location).attr('href',root + '/network/list');
			}catch(e){
				$('#' + id).html('Refresh');
				alert('Error : ' + e.message);
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
			$('#' + id).html('Refresh');
		},
		cache:false,
		headers: { "cache-control": "no-cache" }
	});
}//end function
