<?php

//code.tutsplus.com/tutorials/laravel-4-a-start-at-a-restful-api-updated--net-29785
class PageController extends BaseController {


/**
 * Display a listing of the resource.
 *
 * @return Response
 */
 //GET 
public function index()
{
 
    $pages = Page::where('uid', Auth::user()->uid)->get();
    return Response::json(array(
        'error' => false,
        'pages' => $pages->toArray()),
        200
    );
}


//update all
//PUT : with param
public function update($param)
{
	$affectedRows = Page::where('uid', '=',  Auth::user()->uid)->delete();
	return $this->store();
}

//create
//POST 
public function store()
{
	$affectedRows = Page::where('uid', '=',  Auth::user()->uid)->delete();
	$facebook = new Facebook(Config::get('facebook'));
 	$pages = $facebook->api("/me/accounts", "GET", array("access_token"=> Auth::user()->access_token ));
	foreach($pages['data'] as $pge){
		if(isset($pge['perms'])){
			$page = new Page;
			$page->name = $pge['name'];
			$page->uid = Auth::user()->uid;
			$page->pid = $pge['id'];
			$page->photo = "http://graph.facebook.com/{$pge['id']}/picture";
			$page->page_access_token = $pge['access_token']; 
			$page->save();
		}
	}//end for
	$pages = Page::where('uid', Auth::user()->uid)->get();
	return Response::json(array(
		'error' => false,
		'pages' => $pages->toArray()),
		200
	);
}


 public function missingMethod($parameters = array())
{
	return Response::json(array(
        'error' => true,
        'message' => 'method not exists.'),
        200
    );
}


}//end class