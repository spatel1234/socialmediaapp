<?php
require_once('utility/Utility.php');

class FlickrController extends BaseController {

	private $apiKey;
	private $apiURL;
	private $limit = 20;

	public function __construct()
	{
		$flickr_config = Config::get('flickr');
		$this->apiKey = $flickr_config['apiKey'];
		$this->apiURL = $flickr_config['apiURL'];
	}

	//search flickr 
	//http://localhost/socialmediaapp/public/api/v1/flickr/search?tags=snow,maria&limit=20&page=1
	public function search()
	{
		try{
			$tags			= Input::get('tags');
			$page			= Input::get('page');
			$limit			= Input::get('limit');
			$page			= empty($page) ? 1 : $page;
			$limit			= empty($limit) ? $this->limit : $limit;
			$params			= array('method' => 'flickr.photos.search','tags' => $tags, 'page' => $page, 'per_page' => $limit, 'media' => 'photos', 'extras' => 'views,media,url_n,url_c' );
			$url			= $this->apiURL . '?api_key=' . $this->apiKey;
			$xml_data		= Utility::makeCall($url,$params);
			$result = Formatter::make($xml_data, 'xml')->to_array();  //https://github.com/SoapBox/laravel-formatter
			return Response::json(array(
				'error' => false,
				'results' => $result),
				200
			);
		}catch(Exception $e){
			return Response::json(array(
				'error' => true,
				'results' => $e->getMessage()),
				403
			);
		}
	}//end function

	public function missingMethod($parameters = array())
	{
		return Response::json(array(
			'error' => true,
			'message' => 'Invalid URL.(method not exists).'),
			200
		);
	}//end function

}//end class