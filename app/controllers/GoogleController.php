<?php
require_once('utility/Utility.php');

class GoogleController extends BaseController {

	private $apiKey;
	private $apiURL;
	private $limit = 20;

	public function __construct()
	{
		$google_config = Config::get('google');
		$this->apiKey = $google_config['apiKey'];
		$this->apiURL = $google_config['apiURL'];
	}

	//search flickr 
	//http://localhost/socialmediaapp/public/api/v1/youtube/search?tags=megan&next_page_token=CDwQAA
	public function search()
	{
		try{
			$tags			= Input::get('tags');
			$pageToken		= Input::get('next_page_token');
			$limit			= Input::get('limit');
			$pageToken		= empty($pageToken) ? '' : $pageToken;
			$limit			= empty($limit) ? $this->limit : $limit;

			$params			= array('part' => 'snippet','q' => $tags, 'videoDuration' => 'any', 'maxResults' => $limit, 'order' => 'relevance', 'type' => 'video', 'pageToken' => $pageToken);
			$url			= $this->apiURL . '?key=' . $this->apiKey;
			$json_data		= Utility::makeCall($url,$params);
			$result = Formatter::make($json_data, 'json')->to_array();  //https://github.com/SoapBox/laravel-formatter
			return Response::json(array(
				'error' => false,
				'results' => $result),
				200
			);
		}catch(Exception $e){
			return Response::json(array(
				'error' => true,
				'results' => $e->getMessage()),
				403
			);
		}
	}//end function

	public function missingMethod($parameters = array())
	{
		return Response::json(array(
			'error' => true,
			'message' => 'Invalid URL.(method not exists).'),
			200
		);
	}//end function

}//end class