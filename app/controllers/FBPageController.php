<?php

class FBPageController extends BaseController {

public function lists()
{
	$network_id		=	Input::get('network_id');
	$type			=	Input::get('type'); //page,group,etc,etc
	$result			=	Stream::where('network_id', '=', $network_id)->where('type' , '=', $type)->get();
	return $result;
}


//update all
//PUT : with param
public function update()
{
	return $this->add();
}


public function add()
{
	$network_id		=	Input::get('network_id');
	$type			=	Input::get('type'); //page,group,etc,etc
	
	$network		=	Network::where('id', '=', $network_id)->first();
	$network		=	$network->toArray();
	$affectedRows	=	Stream::where('network_id', '=', $network_id)->where('type' , '=', $type)->delete();
	$facebook		=	new Facebook(Config::get('facebook'));
	$pages = $facebook->api("/me/accounts", "GET", array("access_token"=> $network['access_token']));
	foreach($pages['data'] as $pge){
		if(isset($pge['perms'])){
			$stream = new Stream;
			$stream->name = $pge['name'];
			$stream->type = 'page';
			$stream->user_id = Auth::user()->id;
			$stream->uid = $pge['id'];
			$stream->network_id = $network_id;
			$stream->photo = "http://graph.facebook.com/{$pge['id']}/picture";
			$stream->stream_token = $pge['access_token']; 
			$stream->save();
		}
	}//end for
	$result	=	Stream::where('network_id', '=', $network_id)->where('type' , '=', $type)->get();
	return $result;
}//end function


 public function missingMethod($parameters = array())
{
	return Response::json(array(
        'error' => true,
        'message' => 'method not exists.'),
        200
    );
}


}//end class