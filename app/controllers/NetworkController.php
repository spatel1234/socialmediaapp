<?php

class NetworkController extends BaseController {


	public function __construct()
	{
	}

	public function add(){
		$this->layout->content = View::make('network.add');
	}//end function



	//http://localhost/socialmediaapp/public/api/v1/networks
	//get all networks of loggedin users : API function
	public function index()
	{
		try{
			$networks = $this->_getNetworks();
			if( count($networks) == 0 ){
				return Response::json(array(
					'error' => true,
					'message' => 'No network added.',
					'results' => $networks),
					200
				);
			}
			return Response::json(array(
				'error' => false,
				'results' => $networks),
				200
			);
		}catch(Exception $e){
			return Response::json(array(
				'error' => true,
				'results' => $e->getMessage()),
				403
			);
		}
	}//end function

	public function _getNetworks(){
		$networks = DB::table('users')
			->join('networks', 'networks.user_id', '=', 'users.id')
			->join('streams', 'streams.network_id', '=', 'networks.id')
			->select('users.id as user_id','networks.id as network_id','networks.type as network_type', 'streams.name', 'streams.photo' ,'streams.uid', 'streams.type as stream_type')
			->where('users.id', '=', Auth::user()->id)
			->get();
		return $networks;
	}


	public function listnetwork(){
		$this->layout->content = View::make('network.list',array('networks' => $this->_getNetworks()));
	}

	public function refresh($network_id,$uid){
		//adding/updating pages
		$uid			=	trim($uid);
		$network_id		=	trim($network_id);
		$type			=	'page';
		$network		=	Network::where('id', '=', $network_id)->first();
		$network		=	$network->toArray();
		$affectedRows	=	Stream::where('network_id', '=', $network_id)->where('uid' , '=', $uid)->delete();
		$facebook		=	new Facebook(Config::get('facebook'));
		$pages = $facebook->api("/me/accounts", "GET", array("access_token"=> $network['access_token']));
		foreach($pages['data'] as $pge){
			if(isset($pge['perms']) && $pge['id'] == $uid){
				$stream = new Stream;
				$stream->name = $pge['name'];
				$stream->type = 'page';
				$stream->user_id = Auth::user()->id;
				$stream->uid = $pge['id'];
				$stream->network_id = $network_id;
				$stream->photo = "http://graph.facebook.com/{$pge['id']}/picture";
				$stream->stream_token = $pge['access_token']; 
				$stream->save();
			}
		}//end for
		return Response::json(array(
			'error' => true,
			'results' => $stream->id),
			200
		);
		//end adding/updating pages
	}//end function

	public function missingMethod($parameters = array())
	{
		return Response::json(array(
			'error' => true,
			'message' => 'Invalid URL.(method not exists).'),
			200
		);
	}//end function

}//end class