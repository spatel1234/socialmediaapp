<?php

class StreamController extends BaseController {


/**
 * Display a listing of the resource.
 *
 * @return Response
 */
 //GET 
//http://localhost/socialmediaapp/public/api/v1/streams?network_id=1&type=page
public function index()
{
	try{
		$network_id		=	Input::get('network_id');
		$type			=	Input::get('type'); //page,group,etc,etc
		$stream			=	StreamFactory::makeStream($type);
		$result = $stream->lists();
		return Response::json(array(
			'error' => false,
			'pages' => $result->toArray()),
			200
		);
	}catch(Exception $e){
		$message = $e->getMessage();
		return Response::json(array(
			'error' => true,
			'pages' => array($message)),
			200
		);
	}
}//end function


//update all
//PUT : with param
//http://localhost/socialmediaapp/public/api/v1/streams/update?network_id=1&type=page
public function update($param)
{
	try{
		$network_id		=	Input::get('network_id');
		$type			=	Input::get('type'); //page,group,etc,etc
		$stream			=	StreamFactory::makeStream($type);
		$result = $stream->update();
		return Response::json(array(
			'error' => false,
			'pages' => $result->toArray()),
			200
		);
	}catch(Exception $e){
		$message = $e->getMessage();
		return Response::json(array(
			'error' => true,
			'pages' => array($message)),
			200
		);
	}
}

//create
//POST 
//http://localhost/socialmediaapp/public/api/v1/streams?network_id=1&type=page
public function store()
{
	try{
		$network_id		=	Input::get('network_id');
		$type			=	Input::get('type'); //page,group,etc,etc
		$stream			=	StreamFactory::makeStream($type);
		$result = $stream->add();
		return Response::json(array(
			'error' => false,
			'pages' => $result->toArray()),
			200
		);
	}catch(Exception $e){
		$message = $e->getMessage();
		return Response::json(array(
			'error' => true,
			'pages' => array($message)),
			200
		);
	}
}//end function


 public function missingMethod($parameters = array())
{
	return Response::json(array(
		'error' => true,
		'message' => 'method not exists.'),
		200
	);
}


}//end class


//StreamFactory
class StreamFactory
{
	public static function makeStream($type) //page,group,etc,etc
	{
		switch ($type){
			case "page":
				$stream = new FBPageController;
			break;
			case "group":
				$stream = new GroupController;
			break;
			default:
			break;
		}
		return $stream;
	}
}//end factory class