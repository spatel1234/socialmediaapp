<?php
require_once('utility/Utility.php');

class InstagramController extends BaseController {

	public function __construct()
	{
	}

	//search instagram 
	//http://localhost/socialmediaapp/public/api/v1/instagram/search?tags=snow&limit=5&page=1213323232
	//page = max_tag_id
	public function search()
	{
		try{
			$keyword	= Input::get('tags');
			$limit		= Input::get('limit');
			$page		= Input::get('page');

			$limit = empty($limit) ? 20 : $limit;
			$instagram_config = Config::get('instagram');
			
			//$instagram = new Instagram($instagram_config['clientId']);
			//$photos = $instagram->getTagMedia($keyword);
			//$result = $instagram->pagination($photos,$limit);

			$url		=	"https://api.instagram.com/v1/tags/{$keyword}/media/recent?client_id={$instagram_config['clientId']}&max_id=$page&count=$limit";
			$result		=	Utility::makeCall($url);
			$result		=	json_decode($result);
			return Response::json(array(
				'error' => false,
				'results' => $result),
				200
			);
		}catch(Exception $e){
			return Response::json(array(
				'error' => true,
				'results' => $e->getMessage()),
				403
			);
		}
	}//end function



	public function missingMethod($parameters = array())
	{
		return Response::json(array(
			'error' => true,
			'message' => 'Invalid URL.(method not exists).'),
			200
		);
	}//end function

}//end class