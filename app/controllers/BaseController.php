<?php

class BaseController extends Controller {

	protected $layout  =  'layouts.master';


	public function __construct(){
		// Share a var with all views
		//View::share('myvar', 'some value');
	}

	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}