<?php
class Utility{

	static function makeCall($url,$params = null){
		if (isset($params) && is_array($params)){
		  $paramString = '&' . http_build_query($params);
		} else {
		  $paramString = null;
		}
		$apiCall = $url. $paramString;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apiCall);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$xmlData = curl_exec($ch);
		curl_close($ch);
		if (false === $xmlData){
			throw new Exception("Error: _makeCall() - cURL error: " . curl_error($ch));
		}
		return $xmlData;
	}//end function

}//end class