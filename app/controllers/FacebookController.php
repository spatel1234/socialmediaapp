<?php

class FacebookController extends BaseController {

public function __construct()
{
}


//START API

//post mutiple photo to facebook : POST
//URL = http://localhost/socialmediaapp/public/api/v1/facebook/post?page_id=167697766751212&img_urls[]=http://www.hdpaperwall.com/wp-content/uploads/2013/11/Jessica-Biel-1600X1200-1473.jpg&img_urls[]=http://www.hdpaperwall.com/wp-content/uploads/2013/11/Jessica-Biel-1600X1200-1473.jpg&src_urls[]=http://www.hdpaperwall.com/wp-content/uploads/2013/11/Jessica-Biel-1600X1200-1473.jpg&src_urls[]==http://www.hdpaperwall.com/wp-content/uploads/2013/11/Jessica-Biel-1600X1200-1473.jpg&messages[]=Test+API1&messages[]=Test+API2&publish=true&publish_time=131313133&post_type=photo

//Example: 
//http://localhost/socialmediaapp/public/api/v1/facebook/post?stream_id=167697766751212&img_urls[]=http://www.hdpaperwall.com/wp-content/uploads/2013/11/Jessica-Biel-1600X1200-1473.jpg&src_urls[]=http://www.hdpaperwall.com/wp-content/uploads/2013/11/Jessica-Biel-1600X1200-1473.jpg&messages[]=Test+API1&publish=true&post_type=photo&network_id=2

//Params : network_id,stream_id,post_type=video/photo,img_urls[],src_urls[],messages[],thmb_urls[],publish=true/false,publish_time = 1223232323
//if publish = false then must have publish_time attributes,networks[]=
//Later will update publish & publish_time for each items means takes in array
public function post()
{
	try{
		#print_r(Input::all());
		$networks		= Input::get('networks');
		$img_urls		= Input::get('img_urls');
		$src_urls		= Input::get('src_urls');
		$messages		= Input::get('messages');
		$post_type		= Input::get('post_type');
		$thmb_urls		= Input::get('thmb_urls');
		$publish		= Input::get('publish');
		$publish_time	= Input::get('publish_time'); //timestamp

		foreach($img_urls as $k => $img_url){
			$message							= $messages[$k];
			$message							= trim($message);
			$message							= ( $message == '' ) ? 'App Post' : $message;
			$message							.= "\n\n  &#5413; : " . $src_urls[$k]; 
			$message							=  html_entity_decode($message, ENT_COMPAT, "UTF-8");
			$params['message']					= $message;
			
			foreach($networks as $network){
				$stream = DB::table('users')
						->join('networks', 'networks.user_id', '=', 'users.id')
						->join('streams', 'streams.network_id', '=', 'networks.id')
						->select('streams.stream_token')
						->where('users.id', '=', Auth::user()->id)
						->where('streams.uid', '=', $network)
						->first();
				$params['access_token'] = ''.$stream->stream_token;
				if($post_type == 'video'){
					$node							= "$network/feed";
					$params['link']					= $src_urls[$k];
					$params['source']				= $img_url;
					$params['picture']				= $thmb_urls[$k];
				}else{
					$node							= "$network/photos";
					$params['url']					= $img_url;
				}
				$params['published']				= $publish;
				$params['scheduled_publish_time']	= strtotime($publish_time);
				$result[] = $this->_postToFB($node,$params);
			}//end networks
		}//end for img_urls
	}catch(Exception $e){
		$result = $e->getMessage();
		return Response::json(array(
		'error' => true,
		'results' => array($result)),
		200
		);
	}
	return Response::json(array(
		'error' => false,
		'results' => $result),
		200
	);
}//end function



//END API

//for adding network : Auth required
public function login(){
	$facebook = new Facebook(Config::get('facebook'));
	$params = array(
		'redirect_uri' => url('/facebook/add_network'),
		'scope' => array("manage_pages", "publish_stream","photo_upload","email")
	);
	return Redirect::to($facebook->getLoginUrl($params));
}//end function

//fb callback : fb login redirect uri : Auth required
public function add_network(){
	$code = Input::get('code');
	if (strlen($code) == 0) return Redirect::to('/')->with('message', 'Facebook Communication');

	$facebook = new Facebook(Config::get('facebook'));
	$uid = $facebook->getUser();
	if ($uid == 0) return Redirect::to('/')->with('message', 'There was an error');
	$me = $facebook->api('/me');
	$email = $me['email'];
	$fb_logout_url = $facebook->getLogoutUrl();
	Session::put('fb_logout_url', $fb_logout_url);
	$network = Network::whereUid($uid)->first();
	if (empty($network)){
		$network	= new Network;
		$network->type = 'facebook';
		$network->holder_name = $me['first_name'].' '.$me['last_name'];
		$network->email = $me['email'];
		$network->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';
		$network->uid = $uid;
		$network->user_id = Auth::user()->id;
		$network->access_token = $facebook->getAccessToken(); 
		$network->save();
		$text = 'added';
	}else{
		$network->holder_name = $me['first_name'].' '.$me['last_name'];
		$network->email = $me['email'];
		$network->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';
		$network->uid = $uid;
		$network->access_token = $facebook->getAccessToken(); 
		$network->save();
		$text = 'updated';
	}

	//adding/updating pages
	$network_id		=	$network->id;
	$type			=	'page';
	$network		=	Network::where('id', '=', $network_id)->first();
	$network		=	$network->toArray();
	$affectedRows	=	Stream::where('network_id', '=', $network_id)->where('type' , '=', $type)->delete();
	$facebook		=	new Facebook(Config::get('facebook'));
	$pages = $facebook->api("/me/accounts", "GET", array("access_token"=> $network['access_token']));
	foreach($pages['data'] as $pge){
		if(isset($pge['perms'])){
			$stream = new Stream;
			$stream->name = $pge['name'];
			$stream->type = 'page';
			$stream->user_id = Auth::user()->id;
			$stream->uid = $pge['id'];
			$stream->network_id = $network_id;
			$stream->photo = "http://graph.facebook.com/{$pge['id']}/picture";
			$stream->stream_token = $pge['access_token']; 
			$stream->save();
		}
	}//end for
	//end adding/updating pages
	$this->layout->content = View::make('network.add',array('message' => 'Network added successfully.'));
}//end function



//post to facebook
public function _postToFB($node,$params){
	$params = array_filter($params);
	$facebook = new Facebook(Config::get('facebook'));
	try{
		$result = $facebook->api("$node", "POST", $params);
	}catch (FacebookApiException $e){
		$result = $e->getMessage();
	}
	return $result;
}//end function

public function missingMethod($parameters = array())
{
	return Response::json(array(
        'error' => true,
        'message' => 'method not exists.'),
        200
    );
}

}//end class