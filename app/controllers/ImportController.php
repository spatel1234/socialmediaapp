<?php

class ImportController extends BaseController {

	public function __construct()
	{
		parent::__construct();
	}


	public function index($import_type = '')
	{
		$import_type = $import_type != '' ? $import_type : 'flickr';
		$streams = $this->_getStreams();
		$this->layout->content = View::make('import.index',array('streams' => $streams,'import_type' => $import_type, 'breadcrumb_caption' => ucwords($import_type) ));
	}

	private function _getStreams(){
		$streams = DB::table('users')
			->join('networks', 'networks.user_id', '=', 'users.id')
			->join('streams', 'streams.network_id', '=', 'networks.id')
			->select('users.id', 'streams.name', 'streams.uid', 'streams.type')
			->where('users.id', '=', Auth::user()->id)
			->get();
		return $streams;
	}//end function



}//end controller