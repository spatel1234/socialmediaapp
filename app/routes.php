<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	$data = array();
	if (Auth::check()) {
		$data = Auth::user();
	}
	return View::make('user', array('data'=>$data));
});

Route::get('login/fb', function() {
    $facebook = new Facebook(Config::get('facebook'));
    $params = array(
        'redirect_uri' => url('/login/fb/callback'),
        'scope' => array("manage_pages", "publish_stream","photo_upload","email")
    );
    return Redirect::to($facebook->getLoginUrl($params));
});

Route::get('login/fb/callback', function() {
    $code = Input::get('code');
    if (strlen($code) == 0) return Redirect::to('/')->with('message', 'Facebook Communication');
    
    $facebook = new Facebook(Config::get('facebook'));
    $uid = $facebook->getUser();
     
    if ($uid == 0) return Redirect::to('/')->with('message', 'There was an error');
     
    $me = $facebook->api('/me');
	$email = $me['email'];
	$fb_logout_url = $facebook->getLogoutUrl();
	Session::put('fb_logout_url', $fb_logout_url);
	$user = User::whereEmail($email)->first();
    if (empty($user)) {
    	$user = new User;
    	$user->name = $me['first_name'].' '.$me['last_name'];
    	$user->email = $me['email'];
    	$user->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';
		$user->uid = $uid;
		$user->access_token = $facebook->getAccessToken(); 
		$user->save();
    }else{
    	$user->name = $me['first_name'].' '.$me['last_name'];
    	$user->email = $me['email'];
    	$user->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';
		$user->access_token = $facebook->getAccessToken();
		$user->save();
	}
    Auth::login($user);
    return Redirect::to('/')->with('message', 'Logged in with Facebook');
});


Route::get('logout', function() {
    Auth::logout();
    //return Redirect::to('/')->with('message', 'Successfully logged out.');
    return Redirect::to(Session::get('fb_logout_url'));
});



//UI Routes : START
Route::get('home', array('uses' => 'HomeController@index','before' => 'auth')); //base dashboard
Route::get('import/{type}', 'ImportController@index');
Route::get('network/add', 'NetworkController@add');
Route::get('network/list', 'NetworkController@listnetwork');
Route::get('network/refresh/{network_id}/{uid}', 'NetworkController@refresh');
//UI Routes : END



//start : facebook network adding, routes 
Route::get('facebook/login', array('before' => 'auth', 'uses' => 'FacebookController@login')); //to add facebook network
Route::get('facebook/add_network', array('before' => 'auth', 'uses' => 'FacebookController@add_network'));//facebook redirect callback
//end : facebook network adding, routes 


/*****************API Functions Start******************/

// Route group for API versioning : Stream Controller
Route::group(array('prefix' => 'api/v1', 'before'=> 'auth'), function()
{
	Route::resource('streams', 'StreamController');
});

// Route group for API versioning : Network Controller
Route::group(array('prefix' => 'api/v1', 'before'=> 'auth'), function()
{
	Route::get('networks', 'NetworkController@index');
});


// Route group for API versioning : FB pages
Route::group(array('prefix' => 'api/v1/facebook', 'before'=> 'auth'), function()
{
	Route::resource('pages', 'PageController');
	Route::post('post', 'FacebookController@post');
});

// Route group for API versioning : Instagram Search
Route::group(array('prefix' => 'api/v1/instagram'), function()
{
	Route::get('search', 'InstagramController@search');
});

// Route group for API versioning : Flickr Search
Route::group(array('prefix' => 'api/v1/flickr'), function()
{
	Route::get('search', 'FlickrController@search');
});

// Route group for API versioning : Youtube Search
Route::group(array('prefix' => 'api/v1/youtube'), function()
{
	Route::get('search', 'GoogleController@search');
});
