@section('content')


<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="#">Import</a></li>
			<li><a href="{{ url('import/' . $import_type) }}">{{ucwords($breadcrumb_caption)}}</a></li>
		</ol>
	</div>
</div>


			<div id = "schedule-form" style = "display:none">
				<link href="{{ url('css/jquery.datetimepicker.css') }}" rel="stylesheet">
				<script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4 control-label">Schedule Time</label>
						<div class="col-sm-8">
							<input type = "text"  id = "schedule_time" />
							<button type="button" class="btn btn-default" id="image_button"><span class="glyphicon glyphicon-calendar">Cal</span></button>
						</div>
					</div>
					<div class="form-group has-success has-feedback">
						<div class="col-sm-12">
							<button class="btn btn-success" type="button" onclick="$(this).html('Please wait..');ScheduleContent($('#schedule_time').val())" id = "schdule-button">Schedule</button>&nbsp;
							<button class="btn btn-success" type="button" onclick="CloseModalBox();ResetItem()">Cancel</button>
						</div>
					</div>
				</form>
				<script type = "text/javascript">
					$('#schedule_time').datetimepicker({
						format:'Y-m-d H:i'
					});
					$('#schedule_time').datetimepicker('hide'); //support hide,show and destroy command
					$('#image_button').click(function(){
						$('#schedule_time').datetimepicker('show'); //support hide,show and destroy command
					});
				</script>
			</div>



			<!-- <div class="row" style="text-align: center; position: fixed; z-index: 99999;top:20%;left:40%;display:none" id="allbutton"> -->
			<div class="row" style = "display:none" id="allbutton">
				<div class="col-sm-12">
						<div class="form-group"><!-- sendAllModel -->
								<select id="networks" name = "networks[]" multiple="multiple" class="populate placeholder">
									@foreach ($streams as $stream)
										<option value = "{{$stream->uid}}">{{ $stream->name }}</option>
									@endforeach
								</select>
						</div>
						<div class = "clearfix"></div>
						<button onclick="PostContent();" class="btn btn-success" type="button" id="sendallpbutton">Send All</button>
						<button onclick="OpenModalBox('Schedule ' +  $('.selectbox').length + ' message(s)',$('#schedule-form').html(),'');" class="btn btn-success" type="button" id="sendallpbutton">Schedule All</button>
						<button class="btn btn-success" type="button" onclick="return ResetItem()">Cancel</button>
				</div>
			</div>


<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span>Search contents</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content"><!-- start box-content -->
				<h4 class="page-header">Search</h4>
				<form class="form-horizontal" role="form" action = "#" onsubmit = "return false">
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" name = "tags" id = "tags" class="form-control" placeholder="snow" data-toggle="tooltip" data-placement="bottom" title="Search keywords" >
							<input type="hidden" name = "import_type" value = "{{$import_type}}" />
							<input type="hidden" name = "user_id" value = "{{Auth::user()->id}}" />
						</div>
						<div class="col-sm-12">
							<button type="button" class="btn btn-primary btn-label-left" onclick = "$('#import-content').html('');ImportContent($('input[name=tags]').val(),'{{$import_type}}',1)">Search</button>
						</div>
					</div>
				</form>
			</div><!-- end box-content -->
			<div class="col-sm-12" id = "loadmore-import-content" style = "text-align:center;background:white" ></div>
			<div class="col-sm-12" style = "background:white" id = "import-content"></div><!-- end 12 -->
		</div><!-- class box -->
	</div><!-- col 12 -->
</div><!-- end row -->

<script type = "text/javascript">
$(window).scroll(function(e) {
	var top = $('html').scrollTop() || $('body').scrollTop();
	if( ($(window).height() + top ) == $(document).height()) {
		//ImportContent($('#tags').val(),$('input[name=import_type]').val(),parseInt($('#loadmore-import-content').attr('page')));
		ImportContent($('#tags').val(),$('input[name=import_type]').val(),$('#loadmore-import-content').attr('page'));
	}
});
</script>



@stop