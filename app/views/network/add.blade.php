@section('content')
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="#">Network</a></li>
			<li><a href="{{ url('network/add') }}">{{ 'Add Network' }}</a></li>
		</ol>
	</div>
</div>

<div class="well">
	<p class="lead"><a href="{{ url('facebook/login') }}">{{ HTML::image("img/facebook-connect-button.png", "Facebook Connect") }}</a></p>
	@if(!empty($message))
		<p class = "lead">
			{{$message}}
		</p>
	@endif
</div>

@stop
