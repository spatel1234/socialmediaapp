@section('content')
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="#">Network</a></li>
			<li><a href="{{ url('network/add') }}">{{ 'List Network' }}</a></li>
		</ol>
	</div>
</div>

<div class="well">
	@if(!empty($network_id))
		<p class = "lead">
			<a href="javascript:void(0);" onclick = "AddStream({{$network_id}})" >Click to Add Pages</a>
		</p>
	@endif
	<p class="lead"><a href="{{ url('facebook/login') }}">{{ HTML::image("img/facebook-connect-button.png", "Facebook Connect") }}</a></p>
</div>

@stop
