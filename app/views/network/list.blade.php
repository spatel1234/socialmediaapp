@section('content')
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="#">Network</a></li>
			<li><a href="{{ url('network/add') }}">{{ 'List Network(s)' }}</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>List of Network(s)</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content no-padding">
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>Network Name</th>
							<th>Stream ID</th>
							<th>Stream Name</th>
							<th>Stream Type</th>
							<th>Photo</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($networks as $k=> $network)
						<tr>
							<td>{{ ucwords($network->network_type) }}</td>
							<td>{{ $network->uid }}</td>
							<td>{{ $network->name }}</td>
							<td>{{ ucwords($network->stream_type) }}</td>
							<td><img src = '{{ $network->photo}}' /></td>
							<td><a href = "javascript:void(0)"  id  = "refresh_id{{($k+1)}}" onclick = "RefreshNetwork({{$network->network_id}},{{$network->uid}} ,'refresh_id{{($k+1)}}' );" >Refresh</a></td>
						</tr>
					@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>Network Name</th>
							<th>Stream ID</th>
							<th>Stream Name</th>
							<th>Stream Type</th>
							<th>Photo</th>
							<th>Actions</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	//WinMove();
});

</script>



@stop
