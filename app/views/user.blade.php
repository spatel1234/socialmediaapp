@extends('layouts/layout')

@section('content')
	@if(Session::has('message'))
		<div class="alert alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  {{ Session::get('message')}}
		</div>
	@endif

	@if (!empty($data))
	    <div class="media">
	      <a class="pull-left" href="#">
	        <img class="media-object" src="{{ $data['photo']}}" alt="Profile image">
	      </a>
	      <div class="media-body" style = "overflow:scroll;padding:20px;">
	        <h4 class="media-heading">{{{ $data['name'] }}} </h4>
	        <div>Your email is {{ $data['email']}}</div>
	        <div>Last updated  {{ $data['updated_at']}}</div>
	        <div>Access Token {{ $data['access_token']}}</div>
	      </div>
	    </div>
	    <hr>
	    <a href="{{url('logout')}}">Logout</a>
	@else
		<div class="jumbotron">
		    <h1>Social Media App</h1>
		    <p>Created by <a href="#">Sarang</a></p>
		    <p class="text-center">
		      <a class="btn btn-lg btn-primary" href="{{url('login/fb')}}"><i class="icon-facebook"></i> Login with Facebook</a>
		    </p>
		</div>
	@endif

@stop
