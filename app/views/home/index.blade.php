@section('content')
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="{{ url('home') }}">Dashboard</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 text-center page-404">
		<h1>{{$msg}}</h1>
	</div>
</div>
@stop